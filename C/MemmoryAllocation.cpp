// MemmoryAllocation.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <malloc.h>


void CStyle() {
	printf("\nC Style\n");
	int **a;
	a = (int**)malloc(5 * sizeof(int*));
	for (int i = 0; i < 5; i++) {
		a[i] = (int *)malloc(5 * sizeof(int));
	}
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 5; j++) {
			a[i][j] = 5;
		}
	}
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 5; j++) {
			printf("%d\t", a[i][j]);
		}
		putchar('\n');
	}
	for (int i = 0; i < 5; i++) {
		free(a[i]);
	}
	free(a);
}

void CPPStyle() {
	printf("\nC++ Style\n");
	int **a;
	a = new int*[5];
	for (int i = 0; i < 5; i++) {
		a[i] = new int[5];
	}
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 5; j++) {
			a[i][j] = 5;
		}
	}
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 5; j++) {
			printf("%d\t", a[i][j]);
		}
		putchar('\n');
	}
	for (int i = 0; i < 5; i++) {
		delete []a[i];
	}

}

int main()
{
	CStyle();
	CPPStyle();
	return 0;
}