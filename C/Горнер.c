﻿//Схема Горнера

/*
    Перевод числа из n-ой системы счисления в десятичную
*/

#include <stdio.h>
#include <ctype.h>

unsigned Gorner(char *num, unsigned base) {
    unsigned r = 0;
    while (*num)
        r=isdigit(*num)?(r*base+(*num++)-'0'):(r*base+toupper(*num++)-'A'+10);
    return r;
}

int main(int argc, char *argv[]) {
    char num[] = "123abc";
    unsigned base = 16;
    printf("%s (%d) = %d (10)", num, base, Gorner(num, base));
}