program text_file;

uses sysutils;

var
   line : string;
   //Объявляем переменные для работы с текстовыми файлами
   fin, fAppend, fRewrite : text;
begin
  //Для работы с файлами необходимо переменную связать с именем файла
  AssignFile(fin, 'fin.txt');
  AssignFile(fAppend, 'append.txt');
  AssignFile(fRewrite, 'rewrite.txt');
  //Открываем файл на чтение
  Reset(fin);
  {
  Проверяем существование файла append.txt,
  если файл не существует, то создаем его, иначе открываем на
  перезапись.
  }
  if FileExists('append.txt') then
  begin
    //Открыть файл на дозапись
  	Append(fAppend);
  end
  else
  begin
    //Создать файл либо перезаписать
    Rewrite(fAppend);
  end;
  //Открыть файл на перезапись
  Rewrite(fRewrite);

  //Читаем построчно файл до тех пор, пока не встретим конец файла
  while not EOF(fin) do
  begin
  	ReadLn(fin, line);
    WriteLn(fAppend, line);
    WriteLn(fRewrite, line);
  end;

  {
  Обязательно закрыть файлы. Проблема в том, что
  если вы сами это не сделаете, то система все равно
  закроет его по завершению выполнения программы, но
  так как для обтимизации работы диска система сначала пишет
  все в буфер, то вы рискуете потерять часть данных.
  Close() же в свою очередь записывает в файл все, что осталось
  в буфере и уже потом освобождает дескриптор.
  }
  Close(fin);
  Close(fAppend);
  Close(fRewrite);
end.

